<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Http\Client;

/**
 * Testimport shell command.
 */
class TestimportShell extends Shell
{
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $url = "https://pokeapi.co/api/v2/pokemon/";
        $connection = ConnectionManager::get('default');
        $http = new Client();
        $id = $http->get($url);
        $index=$id->getJson()["count"];
        if(empty($id->getJson()))
        {
            $this->quiet("Yop ton truc marche pas ten pis pour toi: COUNT ID NOT SET".$id->getJson()["count"]);
            return;
        }
        else
        {
            for( $i = 1;$i <$index ;$i++)
            {
                $json = $http->get($url.strval($i));
                $poke = $json->getJson();
                $poketab = TableRegistry::getTableLocator()->get('Pokes');
                $article = $poketab->newEntity();
                $article->name = $poke["name"];
                $article->pokedex_number = $poke["id"];
                $article->health = $poke['stats']["5"]["base_stat"];
                $article->attack =$poke['stats']["4"]["base_stat"];
                $article->defense =$poke['stats']["3"]["base_stat"];
                $article->puissance = 50 *rand(1,2);
                if(empty($poke['sprites']["front_default"]))
                {
                    $swapouille = "ET NION";
                }
                else
                {
                    $swapouille =$poke['sprites']["front_default"];
                }
                $article->spirte =  $swapouille;
                $this->quiet("Pokemon inserted : ".$i." " . $article);
                if ($poketab->save($article)) {
                    // L'entity $article contient maintenant l'id
                    $id = $article->id;
                }
                
            }
        }

        
    }
}
