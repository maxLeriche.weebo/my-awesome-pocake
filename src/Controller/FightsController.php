<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Fights Controller
 *
 * @property \App\Model\Table\FightsTable $Fights
 *
 * @method \App\Model\Entity\Fight[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\DresseurPokesTable $DresseurPokes
 *
 * @method \App\Model\Entity\DresseurPoke[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs'],
        ];
        $fights = $this->paginate($this->Fights);

        $this->set(compact('fights'));
    }

    /**
     * View method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs'],
        ]);

        $this->set('fight', $fight);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     
    *public function add()
    *{
    *    $fight = $this->Fights->newEntity();
    *    if ($this->request->is('post')) {
    *        $fight = $this->Fights->patchEntity($fight, $this->request->getData());
    *        if ($this->Fights->save($fight)) {
     *           $this->Flash->success(__('The fight has been saved.'));
*
 *               return $this->redirect(['action' => 'index']);
  *          }
   *         $this->Flash->error(__('The fight could not be saved. Please, try again.'));
    *    }
     *   $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
      *  $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
       * $winnerDresseurs = $this->Fights->WinnerDresseurs->find('list', ['limit' => 200]);
        *$this->set(compact('fight', 'firstDresseurs', 'secondDresseurs', 'winnerDresseurs'));
    *}
    */
    public function add()
    {
        $fight = $this->Fights->newEntity();
        if ($this->request->is('post')) {
            $formData = $this->request->getData();
            if ($formData['first_dresseur_id'] != $formData['second_dresseur_id'])
            {
                $cbt = $this->_retrieveFightWinner($formData);
                $formData['fight_log'] = $cbt;
                $formData["winner_dresseur_id"]= $cbt["winner"]['id'];
                $fight->winner_dresseur_id = $cbt["winner"]['id'];
                $fight = $this->Fights->patchEntity($fight, $formData);
                if ($this->Fights->save($fight)) {
                    $this->Flash->success(__('The fight has been saved.'));

                    return $this->redirect(['action' => 'view/'.$this->getFight($formData),$formData]);
                }
                $this->Flash->error(__('The fight could not be saved. Please, try again.'));
            }
            else
            {
                $this->Flash->error(__('A fight against yourself is not possible. Please choose a different opponent.'));
            }
        }
        $dresseurs = $this->Fights->Dresseurs->find('list', ['limit' => 200]);
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
        $winnerDresseurs = $this->Fights->WinnerDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'dresseurs', 'firstDresseurs', 'secondDresseurs', 'winnerDresseurs'));
    }

    protected function getFight($param)
    {
        $query = TableRegistry::getTableLocator()->get('Fights')->find()->where(['first_dresseur_id =' => $param['first_dresseur_id']])->where(['second_dresseur_id =' => $param['second_dresseur_id']])->where(['winner_dresseur_id =' => $param['winner_dresseur_id']]);
        foreach ($query as $data) {
            return $data['id'];
        }
    }


    public function  _retrieveFightWinner($formData)
    {
        $log_fight = array();
        $trainer1 = array('ID'=>$formData['first_dresseur_id'],"pokes"=>$this->getPokes($formData['first_dresseur_id']),"id"=>$formData['first_dresseur_id']);
        $trainer2 = array('ID'=>$formData['second_dresseur_id'],"pokes"=>$this->getPokes($formData['second_dresseur_id']),"id"=>$formData['first_dresseur_id']);
        $this->Flash->success(__(var_dump($trainer1)));
        if (count($trainer1['pokes']) == 0 && count($trainer2['pokes']) == 0)
        {
            $alea = random_int(0, 1);
            if ($alea == 0)
            {
                $cbt['id'] = $formData['first_dresseur_id'];
            }
            else
            {
                $cbt['id'] = $formData['second_dresseur_id'];
            }
            $cbt['log'] = "Aucun Dresseur n'a de pokémons, on tire au hasard le gagnant.\n";
        }
        else
        {
            if (count($trainer1['pokes']) == 0 || count($trainer2['pokes']) == 0)
            {
                if (count($trainer1['pokes']) == 0)
                    $cbt['id'] = $formData['second_dresseur_id'];
                else
                    $cbt['id'] = $formData['first_dresseur_id'];
                $cbt['log'] = "L'un des dresseurs n'a de pokémons, on considère qu'il abandonne.\n";
            }
            else
            {
                $cbt = $this->fight($trainer1, $trainer2);
            }
        }
        return $cbt;
    }
    public function fight($trainer1,$trainer2)
    {
        $log["winner"]="nobody";
        $hp1 = $trainer1 ["pokes"]["0"]["HP"];
        $hp2 = $trainer2 ["pokes"]["0"]["HP"];
        $count=0;
        $win=-1;
        $boucle = true;
        while($boucle)
        {
            if($hp1>0)
            {
                $dega=$this->DAGA($trainer1,$trainer2);
                $hp1=$hp1-$dega;
                $log[$count] = $trainer2["pokes"]["0"]["name"]." Attaque et laisse ".$trainer1["pokes"]["0"]["name"]." avec ".strval($hp1)." PV";
                $count = $count +1;
                if($hp1>0)
                {
                    if($hp2>0)
                    {
                        $dega=$this->DAGA($trainer2,$trainer1);
                        $hp2=$hp2-$dega;
                        $log[$count] = $trainer2["pokes"]["0"]["name"]." Attaque et laisse ".$trainer2["pokes"]["0"]["name"]." avec ".strval($hp2)." PV";
                        $count = $count +1;
                        if($hp2>0)
                        {
                            
                        }
                        else
                        {
                            $win=0;
                        }
                    }
                    else
                    {
                        $win=0;
                    }
                }
                else
                {
                    $win=1;
                }
            }
            else
            {
                $win=1;
            }
            if($win==1)
            {
                $log[$count] = $trainer1["pokes"]["0"]["name"]." Est mort";
                $log[$count+1] = $this->getTrainer($trainer2["id"])["first_name"]."A gagner";
                $log["size"] =$count+1;
                $boucle=false;
                $log["winner"] = $trainer2;
            }
            elseif($win==0)
            {
                $log[$count] = $trainer2["pokes"]["0"]["name"]." Est mort";
                $log[$count+1] = $this->getTrainer($trainer1["id"])["first_name"]."A gagner";
                $log["size"] =$count+1;
                $boucle=false;
                $log["winner"] = $trainer1;
            }
        }

        return $log;
        

    }

    public function DAGA($attaquer,$attaquant)
    {
        $def = $attaquer["pokes"]["0"]["DEF"];
        $dagata = (mt_rand(0,15)/15)+0.85;
        $dgt = ((2.4*$attaquant["pokes"]["0"]["ATT"]*$attaquant["pokes"]["0"]["PUI"])/($def*50))*$dagata;
        return $dgt;
    }


    protected function getTrainer($id_trainer)
    {
        $query = TableRegistry::getTableLocator()->get('Dresseurs')->find()->where(['id =' => $id_trainer]);
        foreach ($query as $data) {
            $dat = [
                "first_name" => $data['first_name'],
                "last_name" => $data['last_name']
            ];
            return $dat;
        }
    }

    protected function getPokes($id_trainer)
    {
        $output = array();
        $output_id = array();
        $outputtemp = array();
        $compt = 0;
        $query = TableRegistry::getTableLocator()->get('dresseur_pokes')->find()->select(['poke_id', 'is_fav'])->where(['dresseur_id =' => $id_trainer]);
        foreach ($query as $data) {
            if ($data['is_fav'] == true)
            {
                $outputtemp = array();
                $outputtemp[0] = $data['poke_id'];
                for ($i = 1; $i <= count($output_id); $i++)
                {
                    $outputtemp[$i] = $output_id[$i - 1];
                }
                $output_id = $outputtemp;
            }
            else
            {
                $output_id[$compt] = $data['poke_id'];
            }
            $compt++;
        }
        for ($i = 0; $i < count($output_id); $i++)
        {
            $query = TableRegistry::getTableLocator()->get('Pokes')->find()->select(['name', 'attack', 'spirte', 'health', 'defense', 'puissance'])->where(['id =' => $output_id[$i]]);
            foreach ($query as $data){
                $poke = [
                    "name" => $data['name'],
                    "HP" => $data['health'],
                    "ATT" => $data['attack'],
                    "DEF" => $data['defense'],
                    "PUI" => $data['puissance'],
                    "Sprite" => $data['spirte']
                ];
                $output[$i] = $poke;
            }
        }
        return $output;
    }



    /**
     * Delete method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fight = $this->Fights->get($id);
        if ($this->Fights->delete($fight)) {
            $this->Flash->success(__('The fight has been deleted.'));
        } else {
            $this->Flash->error(__('The fight could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
