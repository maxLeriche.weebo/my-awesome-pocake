<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight $fight
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(__('Delete Fight'), ['action' => 'delete', $fight->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fight->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Fights'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fight'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dresseur Pokes'), ['controller' => 'DresseurPokes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New DresseurPokes'), ['controller' => 'DresseurPokes', 'action' => 'add']) ?> </li>
    </ul>
</nav>

<div class="fights view large-9 medium-8 columns content">
    <h3>Combat entre <?= $fight->has('first_dresseur') ? $this->Html->link($fight->first_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->first_dresseur->id]) : '' ?> et <?= $fight->has('second_dresseur') ? $this->Html->link($fight->second_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->second_dresseur->id]) : '' ?></h3>

    <table class="box">
        <tr>
            <th scope="row"><?= __('ID Combat') ?></th>
            <td><?= $this->Number->format($fight->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID Premier Dresseur') ?></th>
            <td><?= $fight->has('first_dresseur') ? $this->Html->link($fight->first_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->first_dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID Second Dresseur') ?></th>
            <td><?= $fight->has('second_dresseur') ? $this->Html->link($fight->second_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->second_dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID Gagnant') ?></th>
            <td><?= $fight->has('winner_dresseur') ? $this->Html->link($fight->winner_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->winner_dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Création') ?></th>
            <td><?= h($fight->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Modification') ?></th>
            <td><?= h($fight->modified) ?></td>
        </tr>
    </table>
</div>
