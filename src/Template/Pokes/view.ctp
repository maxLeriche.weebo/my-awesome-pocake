<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke $poke
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Poke'), ['action' => 'edit', $poke->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Poke'), ['action' => 'delete', $poke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poke->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pokes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Poke'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dresseur Pokes'), ['controller' => 'DresseurPokes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur Poke'), ['controller' => 'DresseurPokes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Fights'), ['controller' => 'Fights', 'action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="pokes view large-9 medium-8 columns content">
    <h3><?= h($poke->name) ?></h3>
    <table class="box">
        <div>
            <img src= <?= $poke->spirte ?> class="card-img-top">
        </div>

        <div class="card-body">
            <h5 class="card-title"><?= h($poke->nom) ?></h5>
            <p class="card-text">Numéro du pokemon : <?= $this->Number->format($poke->pokedex_number) ?></p>
            <p class="card-text">Vie : <?= $this->Number->format($poke->health) ?></p>
            <p class="card-text">Attaque : <?= $this->Number->format($poke->attack) ?></p>
            <p class="card-text">Defense : <?= $this->Number->format($poke->defense) ?></td>
            </p>
            <p class="card-text">
                <td>Créé le : <?= h($poke->created) ?></td>
            </p>
    </table>
    <div class="related">
        <h4><?= __('Related Dresseur Pokes') ?></h4>
        <?php if (!empty($poke->dresseur_pokes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Dresseur Id') ?></th>
                <th scope="col"><?= __('Poke Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Is Fav') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($poke->dresseur_pokes as $dresseurPokes): ?>
            <tr>
                <td><?= h($dresseurPokes->id) ?></td>
                <td><?= h($dresseurPokes->dresseur_id) ?></td>
                <td><?= h($dresseurPokes->poke_id) ?></td>
                <td><?= h($dresseurPokes->created) ?></td>
                <td><?= h($dresseurPokes->modified) ?></td>
                <td><?= h($dresseurPokes->is_fav) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DresseurPokes', 'action' => 'view', $dresseurPokes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DresseurPokes', 'action' => 'edit', $dresseurPokes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DresseurPokes', 'action' => 'delete', $dresseurPokes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
