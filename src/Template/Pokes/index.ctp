<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke[]|\Cake\Collection\CollectionInterface $pokes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Dresseur Pokes'), ['controller' => 'DresseurPokes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur Poke'), ['controller' => 'DresseurPokes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseur', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Fights'), ['controller' => 'Fights', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="pokes index large-9 medium-8 columns content">
    <h3><?= __('List of Pokes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <tbody>
            <div class="box">
                <?php foreach ($pokes as $poke) : ?>
                    <div class="card" style="width: 18rem; margin: 10px">
                        <img src= <?= $poke->spirte ?> class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title"><?= h($poke->nom) ?></h5>
                            <p class="card-text">Numéro du pokemon : <?= $this->Number->format($poke->pokedex_number) ?></p>
                            <p class="card-text">Vie : <?= $this->Number->format($poke->health) ?></p>
                            <p class="card-text">Attaque : <?= $this->Number->format($poke->attack) ?></p>
                            <p class="card-text">Defense : <?= $this->Number->format($poke->defense) ?></td></p>
                            <a href="#" class="btn btn-primary"><?= $this->Html->link(__('View'), ['action' => 'view', $poke->id]) ?></a>
                            <a href="#" class="btn btn-primary"><?= $this->Html->link(__(' Edit'), ['action' => 'edit', $poke->id]) ?></a>
                            <a href="#" class="btn btn-primary"><?= $this->Form->postLink(__(' Delete'), ['action' => 'delete', $poke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poke->id)]) ?></a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

